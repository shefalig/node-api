"use strict";

/*
 * Name: Shefali Gupta
 * Date: May 20, 2021
 * Section: CSE 154 AL
 *
 * This is the JS to implement the page that gives infor mation about the top 4
 * lipsticks of covergirl
 */
(function() {

  window.addEventListener("load", init);

  /**
   * When the window loads it adds the event listener to the button
   */
  function init() {
    id("begin").addEventListener("dblclick", buttonFunc);
  }

  /**
   * gets information from the api
   * @param {String} requestString the url passed to the fetch method
   */
  function fetchGetInfo(requestString) {
    fetch(requestString)
      .then(statusCheck)
      .then((resp) => resp.text())
      .then(processData)
      .catch(handleError);
  }

  /**
   * Adds event listener to each of the buttons calls the required function on the
   * click of each button
   */
  function buttonFunc() {
    id("btn1").addEventListener("click", () => btn1Func());
    id("btn2").addEventListener("click", () => btn2Func());
    id("btn3").addEventListener("click", () => btn3Func());
    id("btn4").addEventListener("click", () => btn4Func());
  }

  /**
   * process data after getting it from the api
   * @param {text} responseJSON text returned by API
   */
  function processData(responseJSON) {
    let descVari = document.createElement("p");
    descVari.textContent = responseJSON;
    document.querySelector("body").appendChild(descVari);
  }

  /**
   * it adds content to the HTML page when button 1 is clicked
   */
  function btn1Func() {
    let requestString = "/brutton1?number=1";
    fetchGetInfo(requestString);
  }

  /**
   * it adds content to the HTML page when button 2 is clicked
   */
  function btn2Func() {
    let requestString = "/brutton2?number=2";
    fetchGetInfo(requestString);
  }

  /**
   * it adds content to the HTML page when button 3 is clicked
   */
  function btn3Func() {
    let requestString = "/brutton3?number=3";
    fetchGetInfo(requestString);
  }

  /**
   * it adds content to the HTML page when button 4 is clicked
   */
  function btn4Func() {
    let requestString = "/brutton4?number=4";
    fetchGetInfo(requestString);
  }

  /**
   * it handles error for the fetch calls
   */
  function handleError() {
    let errVari = document.createElement("p");
    errVari.textContent = "error!";
    document.querySelector("body").appendChild(errVari);
  }

  /**
   * it returns the state of the promise or returns an error
   * @param {Promise} response the state of the promise
   * @returns {Promise} it returns the state of the promise
   */
  async function statusCheck(response) {
    if (!response.ok) {
      throw new Error(await response.text());
    }
    return response;
  }

  /**
   * Returns the element that has the ID attribute with the specified value.
   * @param {string} idName - element ID
   * @returns {object} DOM object associated with id.
   */
  function id(idName) {
    return document.getElementById(idName);
  }
})();
