'use strict';

const express = require('express');
const app = express();
let content = ["With CoverGirl Outlast Longwear Lipstick you get both moisture and colour! ",
"CoverGirl Outlast Lipcolor Moisturizing Clear Topcoat is a moisturizing lip gloss that keeps" +
"lips feeling moist, soft & smooth.Features: Moisturizing" +
"formulaSafe for sensitive skinFragrance freeIngredients", "CoverGirl Colorlicious" +
"Lipstick gives you rich, satisfying color in shades you can't help but crave!Features:" +
"Deliciously rich color infused with shea butterFull coverage color, high shine intensityNatural" +
"butters help keep lips suppleLasting colourIngredients", "CoverGirls very popular" +
"longwear lip color just got better! The advanced formula with resilient, brilliant" +
"color and new applicator gives you up to 24 hours of color in just 2 easy, breezy steps.Features"];

let subsetContent = [];

const FOURH = 400;

let number = 0;

const EIGHTTH = 8000;

app.get('/brutton1', (req, res) => {
  func(req, res, 0);
});

app.get('/brutton2', (req, res) => {
  func(req, res, 1);
});

app.get('/brutton3', (req, res) => {
  func(req, res, 2);
});

app.get('/brutton4', (req, res) => {
  func(req, res, 3);
});

/**
 * the function helps the api give a response according to the request
 * @param {/all} req request to the api
 * @param {text} res response of the api
 * @param {int} num number corresponding to the button
 */
function func(req, res, num) {
  number = parseInt(req.query['number']) - 1;
  if (number !== num) {
    res.type("text");
    res.status(FOURH).send("error no option listed for" + req.query['number']);
  } else {
    subsetContent.unshift("you chose the " + (number + 1) + "th option:" + content[number]);
    res.type("text");
    res.send("options viewed so far " + subsetContent);
  }
}

app.use(express.static("public"));
const PORT = process.env.PORT || EIGHTTH;
app.listen(PORT);