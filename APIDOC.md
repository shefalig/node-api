# *FILL IN NAME* API Documentation
a Cover girl API that gives information of the top 4 lipsticks of covergirl

## *Fill in Endpoint 1 Title*
**Request Format:** /all

**Request Type:** GET

**Returned Data Format**: Plain Text

**Description:** Gets the information for first product of cover girl along with information of all
the products selected so far


**Example Request:** /all

**Example Response:**
options viewed so far you chose the 1th option:With CoverGirl Outlast Longwear Lipstick
you get both moisture and colour!

**Error Handling:**
it checks the value of the query number passed in if it does not equal to 1 it gives
a 400 error header and a descriptive message

## *Fill in Endpoint 2 Title*
**Request Format:** /all

**Request Type:** GET

**Returned Data Format**: Plain Text

**Description:** Gets the information for second product of cover girl along with information of all
the products selected so far

**Example Request:** /all

**Example Response:**
options viewed so far you chose the 2th option:CoverGirl Outlast Lipcolor Moisturizing Clear Topcoat is a moisturizing lip gloss that keepslips feeling moist, soft & smooth.Features: MoisturizingformulaSafe for sensitive skinFragrance freeIngredients, you chose the 1th option:With CoverGirl Outlast Longwear Lipstick you get both moisture and colour!

**Error Handling:**
it checks the value of the query number passed in if it does not equal to 2 it gives
a 400 error header and a descriptive message

## *Fill in Endpoint 3 Title*
**Request Format:** /all

**Request Type:** GET

**Returned Data Format**: Plain Text

**Description:** Gets the information for third product of cover girl along with information of all
the products selected so far

**Example Request:** /all

**Example Response:**
options viewed so far you chose the 3th option:CoverGirl ColorliciousLipstick gives
you rich, satisfying color in shades you can't help but crave!Features:Deliciously
rich color infused with shea butterFull coverage color, high shine intensityNaturalbutters
help keep lips suppleLasting colourIngredients,you chose the 2th option:CoverGirl Outlast
Lipcolor Moisturizing Clear Topcoat is a moisturizing lip gloss that keepslips feeling moist,
soft & smooth.Features: MoisturizingformulaSafe for sensitive skinFragrance freeIngredients,
you chose the 1th option:With CoverGirl Outlast Longwear Lipstick you get both moisture and colour

**Error Handling:**
it checks the value of the query number passed in if it does not equal to 3 it gives
a 400 error header and a descriptive message

## *Fill in Endpoint 4 Title*
**Request Format:** /all

**Request Type:** GET

**Returned Data Format**: Plain Text

**Description:** Gets the information for fourth product of cover girl along with information of all
the products selected so far

**Example Request:** /all

**Example Response:**
options viewed so far you chose the 4th option:CoverGirls very popularlongwear lip
color just got better! The advanced formula with resilient, brilliantcolor and new applicator
gives you up to 24 hours of color in just 2 easy, breezy steps.Features,you chose the
3th option:CoverGirl ColorliciousLipstick gives you rich, satisfying color in shades you
can't help but crave!Features:Deliciously rich color infused with shea butterFull coverage
color, high shine intensityNaturalbutters help keep lips suppleLasting colourIngredients,you
chose the 2th option:CoverGirl Outlast Lipcolor Moisturizing Clear Topcoat is a moisturizing
lip gloss that keepslips feeling moist, soft & smooth.Features: MoisturizingformulaSafe for
sensitive skinFragrance freeIngredients, you chose the 1th option:With CoverGirl Outlast
Longwear Lipstick you get both moisture and colour!

**Error Handling:**
it checks the value of the query number passed in if it does not equal to 3 it gives
a 400 error header and a descriptive message
